# Convolutional Neural Network

# Part 1 - Building the CNN

# Importing keras libraries and packages

# Defines the architecture of the layers (sequence of layers or graph)
from keras.models import Sequential
# Meant for the convolution layers    
from keras.layers import Convolution2D  
# Pooling
from keras.layers import MaxPool2D  
# Flatten for the ANN     
from keras.layers import Flatten  
# Creates the layers       
from keras.layers import Dense     

from PIL import Image      


# Initializing the CNN
classifier = Sequential()


# Adding the layers Step 1- First Convolution layer , stride of 1
# Tensorflow bankend uses W, H, C
# Theano bankend C, W, H
classifier.add(Convolution2D(32, (3,3), input_shape=(128, 128, 3), activation='relu'))
# Step 2- MaxPooling, stride of 2
classifier.add(MaxPool2D(pool_size = (2,2)))

# Adding a second convolution layer for better accuracy. input_shape was removed since input will be feature maps
classifier.add(Convolution2D(32, (3,3), activation='relu'))
# Max pooling for the second layer
classifier.add(MaxPool2D(pool_size = (2,2)))

# Adding a third convolution layer for better accuracy. input_shape was removed since input will be feature maps
classifier.add(Convolution2D(64, (3,3), activation='relu'))
# Max pooling for the third layer
classifier.add(MaxPool2D(pool_size = (2,2)))

# Adding a fourth convolution layer for better accuracy. input_shape was removed since input will be feature maps
classifier.add(Convolution2D(128, (3,3), activation='relu'))
# Max pooling for the fourth layer
classifier.add(MaxPool2D(pool_size = (2,2)))


# Adding a fifth convolution layer for better accuracy. input_shape was removed since input will be feature maps
classifier.add(Convolution2D(256, (3,3), activation='relu'))
# Max pooling for the fifth layer
classifier.add(MaxPool2D(pool_size = (2,2)))



# Step 3-Flattening
classifier.add(Flatten())

# Step 4: Full Connection steps

# Full connected layer
classifier.add(Dense(128, activation='relu')) 

# Full connected layer
classifier.add(Dense(128, activation='relu')) 
# Output layer  
# For more than one class the output will have neurons equivalent to the number of classes e.g. 3
classifier.add(Dense(1, activation = 'sigmoid')) 

# Step 5- Compiling the CNN
classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics = ['accuracy'])

# Step 6- Fitting the CNN to the images
# Image augumentation: To prevent overfitting results to good result with training_set and poor result with new dataset

# Overvitting occurs when we have few images and to solve this we can use image augumentation


# Impporting ImageDataGenerator

# rescale keeps the scale to be between 0 and 1 (Scaling)
# Shear range: transforms the images 
# Zoom range: random zoom
# Flipping 
# Target size is the size of the images
# class_mode: binary, 


from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1./255)

training_set = train_datagen.flow_from_directory(
                                                'dataset/training_set',
                                                target_size=(128, 128),
                                                batch_size=32,
                                                class_mode='binary')

test_set = test_datagen.flow_from_directory(
                                            'dataset/test_set',
                                            target_size=(128, 128),
                                            batch_size=32,
                                            class_mode='binary')

classifier.fit_generator(
                            training_set,
                            steps_per_epoch= 8000,
                            epochs= 20,
                            validation_data = test_set,
                            validation_steps = 2000)



classifier.save_weights('cat_dog_classifier.h5')

img_pred = image.load_img('dataset/validation/dogs/dog.10000.jpg', target_size = (64,64))
img_pred = image.img_to_array(img_pred)
img_pred = np.expand_dims(img_pred, axis = 0)

# Run Model

result = classifier.predict(img_pred)
print(result)

if result[0][0] == 1:
    prediction = "Dog"
else:
    prediction = "Cat"
    
print(prediction)

 
