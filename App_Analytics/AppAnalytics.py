"""
@ Model Developer : Young Olutosin
@Brief            : App Analytics
                    
"""

### Importing of Libraries

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from dateutil.parser import parse


# Importing dataset

dataset = pd.read_csv('appdata10.csv')
metadata = pd.read_csv('top_screens.csv')

# Checking data types of all variables
data_type = dataset.dtypes


# Gives more insight about the dataset
dataset.describe()

# Extracting just the first two values
dataset['hour'] = dataset.hour.str.slice(1, 3).astype(int)


## Create a copy and dropping non-numeric columns
dataset2 = dataset.copy().drop(columns = ['user', 'screen_list', 'enrolled_date', 'first_open', 'enrolled'])



## Histograms: Getting insights out distribution


# Visualizing more images
# Dimension of grid
plt.suptitle('Histograms of Numerical Columns', fontsize = 30)
# dataset.shape returns rows and columns
for i in range(1, dataset2.shape[1] + 1):     
    plt.subplot(3, 3, i)
    f = plt.gca()
    # uses the columns as the title
    f.set_title(dataset2.columns.values[i - 1])   
     # squares the entire columns: sets the bins values     
    vals = np.size(dataset2.iloc[:, i -1].unique())   
    plt.hist(dataset2.iloc[:, i - 1], bins = vals, color = 'blue')

# Correlation with response
data = dataset2.corrwith(dataset.enrolled).plot.bar(figsize=(7,7), title = "Correlation with response",
                         fontsize = 7, rot = 45)

# Correlation matrix
sns.set(style = 'white')
corr = dataset2.corr()
mask = np.zeros_like(corr, dtype = bool)
mask[np.triu_indices_from(mask)] = True

f, ax = plt.subplots(figsize = (10,6))
f.suptitle('Correlation Matrix', fontsize = 10)
cmap = sns.diverging_palette(220, 10, as_cmap=True)
sns.heatmap(corr, mask=mask, cmap= cmap, vmax=0.3, center = 0, 
            square=True, linewidths=.5, cbar_kws={"shrink": .5})
    
## Feature Engineering: Finetuning the response variable which is used to set an activator for validating the dataset
dataset['first_open'] = [parse(row_data) for row_data in dataset['first_open']]

# isinstance was used because not all rows were populated
dataset['enrolled_date'] = [parse(row_data) if isinstance(row_data, str) else row_data for row_data in dataset['enrolled_date']]

## Converts to hour
dataset['diff'] = (dataset.enrolled_date - dataset.first_open).astype('timedelta64[h]')
plt.hist(dataset['diff'].dropna(), color = "#3f5d7d")
plt.title('Distribution of difference (first open and enrollment)')

## Converts to hour
dataset['diff'] = (dataset.enrolled_date - dataset.first_open).astype('timedelta64[h]')
plt.hist(dataset['diff'].dropna(), color = "#3f5d7d", range = [0, 100])
plt.title('Distribution of difference of first open and Time of enrollment')

## Sets the diff to a max of 48

dataset.loc[dataset['diff'] > 48, 'enrolled'] = 0
dataset = dataset.drop(columns = ['diff', 'first_open', 'enrolled_date'])
top_screens = pd.read_csv('top_screens.csv').top_screens.values

# adds one comma to the last element in the list
dataset['screen_list'] = dataset.screen_list.astype(str) + ','

# creates a separate column for each element
for sc in top_screens:
    dataset[sc] = dataset.screen_list.str.contains(sc).astype(int)
    dataset['screen_list'] = dataset.screen_list.str.replace(sc+",", "")

dataset["Other"] = dataset.screen_list.str.count(",")
dataset = dataset.drop(columns = ['screen_list'])


# Funneling: combining screens that belong to the same set
savings_screens = ["Saving1",
                  "Saving2",
                  "Saving4",
                  "Saving5",
                  "Saving6",
                  "Saving7",
                  "Saving8",
                  "Saving9",
                  "Saving10"
        
        ]

dataset["SavingsCount"] = dataset[savings_screens].sum(axis = 1)
dataset = dataset.drop(columns = [savings_screens])

cm_screens = ["Credit1",
                  "Credit2",
                  "Credit3",
                  "Credit3",
                  "Credit3Container",
                  "Credit3Dashboard"
                  ]

dataset["CMCount"] = dataset[cm_screens].sum(axis = 1)
dataset = dataset.drop(columns = [cm_screens])

cc_screens = ["CC1",
                  "CC1Category",
                  "CC3"
                  ]

dataset["CCCount"] = dataset[cc_screens].sum(axis = 1)
dataset = dataset.drop(columns = cc_screens)


loan_screens = ["Loan",
                  "Loan2",
                  "Loan3",
                  "Loan4"
                 
                  ]

dataset["LoansCount"] = dataset[loan_screens].sum(axis = 1)
dataset = dataset.drop(columns = loan_screens)

dataset.to_csv('new_appdata10.csv', index = False)