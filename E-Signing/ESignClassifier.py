"""
@ Model Developer : Young Olutosin
@Brief            : Model To Classify User Quality Level for Loan Grant
@Comment          : Dataset are Linearly Separable
@Date             : 5-10-2019
    



@Description      :

                  Target Class:
                               --Qualified       --> 1
                               --Not Qualified   --> 0
                      
                  Dataset     : 
                               --Training_Set
                                           : 212
                                           : 357
                                 Total Instances : 569
                                     
                               --Test_set     --->>>
"""

# Libraries Inclusion
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# Loading Dataset
dataset = pd.read_csv('financial_data.csv')
dataset.describe()
dataset.columns
dataset.isnull().sum()
dataset.isna().sum()

# Visualization
corr = dataset.corr()
plt.figure(figsize=(10,5.5))
sns.heatmap(corr, annot= True, )

dataset2 = dataset.drop(columns = ['entry_id', 'pay_schedule', 'e_signed'])
dataset2.corrwith(dataset.e_signed).plot.bar(figsize=(10,5.5), title="Correction With E Signed", fontsize=9, rot = 45, grid = True)






