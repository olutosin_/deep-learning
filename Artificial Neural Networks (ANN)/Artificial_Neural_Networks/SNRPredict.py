# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 16:32:39 2019

@author: Young Olutosin
"""

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_excel('data.xlsx')
