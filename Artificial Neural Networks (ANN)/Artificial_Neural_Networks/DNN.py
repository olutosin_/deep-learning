# Artificial Neuaral Network

# Installing Theano: open source Numerical computation library and can run on CPU and GPU
# pip install -upgrade --no-deps git+git://github.com/Theano/Theano.git


# Installing TensorFlow: Developed by Google and now on Apache2.0
# This can be installed from the website: https://docs.anaconda.com/anaconda/user-guide/tasks/tensorflow/

# Installing Keras: Combines both Theano and TensorFlow
# pip install --upgrade keras

# Part 1 - Data Preprocessing

# Classification template

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Churn_Modelling.csv')
X = dataset.iloc[:, 3:13].values
y = dataset.iloc[:, 13].values

# This converts an object into an array of datatype int64
#K = np.array(X, dtype = "int64")

# Encoding the categorical Independent Variable
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X_1 = LabelEncoder()
X[:, 1] = labelencoder_X_1.fit_transform(X[:, 1])

labelencoder_X_2 = LabelEncoder()
X[:, 2] = labelencoder_X_2.fit_transform(X[:, 2])

# Creates the dummy variable
onehotencoder = OneHotEncoder(categorical_features = [1])
X = onehotencoder.fit_transform(X).toarray()

# Removes one of the dummy variables created to avoid the dummy trap
# There is no need to create a dummy variable for gender
X = X[:, 1:]

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20, random_state = 0)


# Feature Scaling is very important in ANN
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# ANN

# Importing Keras library and packages
import keras
from keras.models import Sequential
from keras.layers import Dense

# Initializing the ANN model
# This is an object of the classifier as a sequence of layer
classifier = Sequential()

# Defining the input layer and first hidden layer
# kernel_initializer could be "uniform or glorot_uniform" which initilizes the weights to values closer to zero
classifier.add(Dense(6, input_shape=(11,), kernel_initializer='glorot_uniform', activation = 'relu'))

# Second hidden layer
classifier.add(Dense(6, kernel_initializer='glorot_uniform', activation = 'relu'))

# Output layer
# for binary class we use sigmoid
# for multiclass  e.g. 3, 1 will be set as 3 and activation will be set as softmax built on sigmoid
classifier.add(Dense(1, kernel_initializer='glorot_uniform', activation = 'sigmoid'))


# Compiliation using SGD of whole ANN
# SBD could be adam 
# loss or cost function for binary class is binary_crossentropy 
# and for multiple will be categorical_crossentropy and both are lagarithmic function

classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# Training ANN 
classifier.fit(X_train, y_train, batch_size = 10, epochs = 100)


# Predicting the Test set results
y_pred = classifier.predict(X_test)
# This returns Yes if it is and No is it is not i.e. 1 or 0
y_pred = (y_pred > 0.5) # where 0.5 is threshold and we could use a higher threshold if the information is sensitive


# Model Evaluation
# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix, precision_score, accuracy_score, recall_score, f1_score, cohen_kappa_score
cm = confusion_matrix(y_test, y_pred)
acc = accuracy_score(y_test, y_pred)
print(acc)