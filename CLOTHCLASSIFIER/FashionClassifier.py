"""
@ Model Developer : Young Olutosin
@Brief            : Model To Classify a person's outfit. "A Reference Model for Image Classification Problem"
@Business Use Case: Target Marketting For More Income Stream
@Comment          : Dataset is Non Linear
@Date             : 5-10-2019
@Image            : Greyscale Images (28 by 28) ---> 784 
@Source           : MNIST Image dataset: git clone git@github.com:zalandoresearch/fashion-mnist.git
@@KernelSource    : setosa.io/ev/image-kernels
@Reference        : Deep Fashion Database: http://mmlab.ie.cuhk.edu.hk/projects/DeepFashion.html
@Description      :

                  Target Classes:
                               -- T-shirt/top -->  0
                               -- Trouser     -->  1
                               -- Pullover    -->  2
                               -- Dress       -->  3
                               -- Coat        -->  4
                               -- Sandal      -->  5
                               -- Shirt       -->  6
                               -- Sneaker     -->  7
                               -- Bag         -->  8
                               -- Ankle boot  -->  9
                
                      
                  Dataset     : 
                               --Training_Set 
                                          ---> 60000 Images
                               --Test_set
                                          ---> 10000 Images
                
"""

# Importing of Libraries
import pandas as pd
import numpy as np
from numpy.random import random
import seaborn as sns
import matplotlib.pyplot as plt
import mnist_reader



# Importing dataset
X_train, y_train = mnist_reader.load_mnist('data/fashion', kind='train')
X_test, y_test = mnist_reader.load_mnist('data/fashion', kind='t10k')


y_train = y_train.reshape(-1,1)
training = np.append(X_train, y_train, axis = 1)
training = np.array(X_train, dtype='float32')
test = np.array(X_test, dtype='float32')

# Visualizing the Image: Note: It needs reshape

plt.imshow(training[1710, 0:].reshape(28,28))

# Visualizing more images

# Dimension of grid
W_grid = 15
L_grid = 15

fig, axes = plt.subplots(L_grid, W_grid, figsize=(17,17))

axes = axes.ravel() # flattens 15 by 15 to 225

n_training = len(training)

for i in np.arange(0, W_grid * L_grid):
    # Randomize the printing
    index = np.random.randint(0, n_training)
    #read and display image with selected index
    axes[i].imshow(training[index, 0:].reshape(28,28))
    axes[i].set_title(y_train[index, 0], fontsize =8)
    axes[i].axis('off')
plt.subplots_adjust(hspace = 0.4)



# Normalizing data
X_train = X_train[: , 0:]/255
y_train = y_train

X_test = X_test[: , 0:]/255
y_test = y_test



# Converting image array into format CNN can use
from sklearn.model_selection import train_test_split
X_train, X_validate,Y_train, Y_validate = train_test_split(X_train, y_train, test_size = 0.2, random_state = 12345)

# Reshaping
X_train = X_train.reshape(X_train.shape[0], *(28,28,1))
X_validate =X_validate.reshape(X_validate.shape[0], *(28,28,1))
X_test = X_test.reshape(X_test.shape[0], *(28,28,1))




# Creating the layers
import keras
from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import Dense

classifier = Sequential()

# Creating first convolution layer
classifier.add(Convolution2D(32, (3,3), input_shape=(28, 28, 1), activation='relu'))
# Step 2- MaxPooling, stride of 2
classifier.add(MaxPooling2D(pool_size = (2,2)))


# Second convolution layer
# Second poling layer

# Flatten layer
classifier.add(Flatten())

# Fully Connected layer: sparse_categorical crossentropy
classifier.add(Dense(32, activation='relu')) 
classifier.add(Dropout(0.5)) 

# Fully connected for all classes: 10 Neurons
classifier.add(Dense(10, activation='sigmoid')) 



# Compiling the model
from keras import optimizers
Adam = optimizers.adam(lr = 0.001)
classifier.compile(optimizer='adam' , loss='sparse_categorical_crossentropy', metrics = ['accuracy'])

# Fitting the model
classifier.fit(X_train, Y_train, batch_size=512, verbose = 1, epochs = 50, validation_data = (X_validate, Y_validate))

# Model Evaluation
Evaluation = classifier.evaluate(X_test, y_test)
accuracy = Evaluation[1]
print("Accuracy of this model is " + format(accuracy * 100, "2.2f") + "%")

predicted_class = classifier.predict_classes(X_test)
print(predicted_class)


# Visualizing Prediction
L = 5
W = 5

fig, axes = plt.subplots(L, W, figsize = (12, 12))
# Flattened the array
axes = axes.ravel()
for i in np.arange(0, L * W):
    
    #read and display image with selected index
    axes[i].imshow(X_test[i].reshape(28,28))
    #title = "Predicted class" + format(predicted_class[i], "2.2f") + "True class" + format(y_test[i], "d")
    axes[i].set_title("Predicted class " + format(predicted_class[i], "2.1f") + "\nTrue Class" + format(y_test[i], "2.1f"))
    axes[i].axis('off')
    
plt.subplots_adjust(hspace = 0.7)


# Model Evaluation using cm
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, predicted_class)
plt.figure(figsize=(20, 10))
sns.heatmap(cm, annot=True)

from sklearn.metrics import classification_report
num_classes = 10
target = []
for i in range(num_classes):
    target_names = "Class " + format(i)
    target.append(target_names)
 
print(classification_report(y_test, predicted_class, target_names = target))
report = classification_report(y_test, predicted_class, target_names = target)
classifier.summary()
