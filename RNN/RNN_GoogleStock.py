# Recurrent Neuaral Network
# Dataset Beginning 1st of Jan 2012 - End of Dec 2016
# ALl models are wrong while some are wrong

# Imorting libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# Importing dataset
training_set = pd.read_csv("Google_Stock_Price_Train.csv")

# 1:2 converts it into an array
training_set = training_set.iloc[:, 1:2].values
training_set.shape

# Feature Scaling
# In LSTM, hyperbolic tangent activation function is used and would make
# sense to use noramlization instead of of standardization
from sklearn.preprocessing import MinMaxScaler
sc = MinMaxScaler()
training_set = sc.fit_transform(training_set)

# Getting the inputs and outputs
X_train = training_set[0:1257]  # data at time t
y_train = training_set[1: 1258] # data at time t + 1 i.e. shifting by 1

# Reshaping to the data that will be taken by keras
# Source: https://keras.io/layers/recurrent/: batch size, timesteps and number of features
X_train = np.reshape(X_train, (1257, 1, 1))

#Initializaing RNN
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM

# Initia;izing RNN
regressor = Sequential()

regressor.add(Dense())

# input_shape: timesteps and number of input features
regressor.add(LSTM(units = 4, activation='sigmoid', input_shape=(None, 1)))


# Adding output layer
regressor.add(Dense(units = 1))

# Compiling RNN
regressor.compile(optimizer = 'adam', loss = 'mse')

#Fitting the model
regressor.fit(x=X_train, y=y_train, batch_size=32, epochs=200)


# Making the prediction
test_set = pd.read_csv("Google_Stock_Price_Test.csv")
real_stock_price = test_set.iloc[:, 1:2].values
inputs = real_stock_price
inputs = sc.transform(inputs)

# Reshaping the inputs
inputs = np.reshape(inputs, (20, 1, 1))

predicted_stock_price = regressor.predict(inputs)
predicted_stock_price = sc.inverse_transform(predicted_stock_price)


# Visualizing the result
axes= plt.axes()
axes.set_xlim([1,20])
axes.set_xticks([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20])
plt.plot(real_stock_price, c = 'red', label = 'Real_Google_Stock_Price')
plt.plot(predicted_stock_price, c = 'green', label = 'Predicted_Google_Stock_Price')
plt.title("Google Stock Price Prediction")
plt.xlabel("Predicted Result for 20 Days")
plt.ylabel("Opening Stock Price")
plt.legend()
plt.show()


# Evaluating the model
import math
from sklearn.metrics import mean_squared_error
rmse = math.sqrt(mean_squared_error(real_stock_price, predicted_stock_price))
mean_ = real_stock_price.mean()
performance_measure = rmse/mean_


print(regressor.history.keys())
# summarize history for accuracy
plt.plot(regressor.history['accuracy'])
plt.plot(regressor.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
# summarize history for loss
plt.plot(regressor.history['loss'])
plt.plot(regressor.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()




